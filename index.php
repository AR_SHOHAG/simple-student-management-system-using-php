
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/style.css">

    <style>
        footer {
            padding: 1em;
            color: white;
            background-color: #1B4F72;
            clear: left;
            text-align: center;
        }

    </style>


</head>
<body>
<hr>
<h1 style="text-align: center;">Student Management System</h1>
<hr>

<div class="container" >
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="add_course.php">Add Course</a></li>
                        <li><a href="add_student.php">Add Student</a></li>
                        <li><a href="all_student.php">All Students</a></li>
                        <li><a href="all_course.php">All Courses</a></li>
                        <li><a href="assign.php">Add Student & Courses</a></li>
                    </ul>

                    <form class="navbar-form navbar-left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <img src="image/a.jpg" alt="Mountain View" style="width:1140px;height:540px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <footer>Copyright &copy; Md. Arifur Rahman</footer>
        </div>
    </div>
</div>

</body>




</html>