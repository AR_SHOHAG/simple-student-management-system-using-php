<?php
$db = new PDO('mysql:host=localhost;dbname=project1;charset=utf8mb4', 'root', '');

$id = $_GET['id'];

$query = "SELECT * FROM `student_info` WHERE `id` = '$id'";

$stmt = $db->query($query);
$data = $stmt->fetch(PDO::FETCH_ASSOC);







?>

<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">


    <style>
        footer {
            padding: 1em;
            color: white;
            background-color: #1B4F72;
            clear: left;
            text-align: center;
        }

    </style>


</head>
<body>
<hr>
<h1 style="text-align: center;">Student Management System</h1>
<hr>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="add_course.php">Add Course</a></li>
                        <li><a href="all_student.php">Add Student</a></li>
                        <li><a href="all_student.php">All Students</a></li>
                        <li><a href="all_course.php">All Courses</a></li>
                        <li><a href="assign.php">Course Registration</a></li>
                    </ul>

                    <form class="navbar-form navbar-left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>


<h2>Modify Student Information</h2>

<form action="student_added.php" method="POST" enctype="multipart/form-data">
    <div class="box">
        <hr>
        <div class="form-group">
            <label for="name">Enter Student's Name:</label>
            <input type="text" name="name" value="<?php echo $data['name']; ?>" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Enter Student's Email:</label>
            <input id="email" type="email" name="email" value="<?php echo $data['email']; ?>" class="form-control">
        </div>
        <div class="form-group">
            <label for="pass">Enter Student's Id:</label>
            <input type="text" name="id" value="<?php echo $data['id']; ?>" class="form-control">

        </div>
        <div class="form-group">
            <label for="address">Enter Student's Address:</label>
            <textarea id="address" name="address"  class="form-control"><?php echo $data['address']; ?></textarea>
        </div>
        <div class="form-group">
            <label for="cell">Enter Student's Cell:</label>
            <input id="cell" type="text" name="cell" value="<?php echo $data['cell']; ?>" class="form-control">
        </div>

        <div class="form-group">
            <label>Select Gender:</label>
            <input id="male" type="radio" <?php echo ($data['gender'] == 'Male')? 'checked':''; ?> name="gender"  value="Male">
            <label for="male">Male</label>

            <input id="female" type="radio" <?php echo ($data['gender'] == 'Female')? 'checked':''; ?> name="gender" value="Female">
            <label for="female">Female</label>
        </div>

        <div class="form-group">
            <label for="image">Student Image</label>
            <input id="image" type="file" name="image" class="btn btn-default">
        </div>

        <?php
            $date = explode('-', $data['dob']);
            //var_dump($date);
        ?>


        <div class="form-group">
            <label>Date of Birth:</label>
            <select name="day" class="btn btn-default">
                <option value="">Day</option>
                <?php
                    for($i = 1; $i < 32; $i++){
                        if($i == $date[0])
                            $key = 'selected';
                        else
                            $key = '';

                        echo "<option $key value='$i'>$i</option>";
                    }

                ?>

            </select>
            <select name="month" class="btn btn-default">
                <option value="">Month</option>
                <?php
                    for($i = 1; $i < 13; $i++){
                        if($i == $date[1])
                            $key = 'selected';
                        else
                            $key = '';

                        echo "<option $key value='$i'>$i</option>";
                    }

                ?>
            </select>
            <select name="year" class="btn btn-default">
                <option value="">Year</option>
                <?php
                    for($i = 1990; $i < 2018; $i++){
                        if($i == $date[2])
                            $key = 'selected';
                        else
                            $key = '';

                        echo "<option $key value='$i'>$i</option>";
                    }

                ?>
            </select>
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Confirm" class="btn btn-success">
            <input type="reset" name="submit" value="Reset" class="btn btn-info">
        </div>
    </div>
</form>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <footer>Copyright &copy; Md. Arifur Rahman</footer>
        </div>
    </div>
</div>

</body>
</html>
