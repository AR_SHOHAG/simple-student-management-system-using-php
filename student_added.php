<?php
$db = new PDO('mysql:host=localhost;dbname=project1;charset=utf8mb4', 'root', '');
$_POST['image'] = $_FILES['image']['name'];
$_POST['dob'] = $_POST['day'].' - '.$_POST['month'].' - '.$_POST['year'];


$name = $_FILES['image']['name'];
$tmp_name = $_FILES['image']['tmp_name'];
$uploads_dir = 'uploads/';
$path = $uploads_dir.$name;

move_uploaded_file($tmp_name, $path);



$query = "INSERT INTO `student_info` (`name`, `email`, `id`, `address`, `cell`, `gender`, `image`, `dob`) VALUES ('".$_POST['name']."', '".$_POST['email']."', '".$_POST['id']."', '".$_POST['address']."', '".$_POST['cell']."', '".$_POST['gender']."', '".$_POST['image']."', '".$_POST['dob']."');";

$result = $db->exec($query);

if($result){
    echo "Student Added Successfully";
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">


    <style>
        footer {
            padding: 1em;
            color: white;
            background-color: #1B4F72;
            clear: left;
            text-align: center;
        }

    </style>


</head>
<body>
<hr>
<h1 style="text-align: center;">Student Management System</h1>
<hr>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="add_course.php">Add Course</a></li>
                        <li><a href="all_student.php">Add Student</a></li>
                        <li><a href="all_student.php">All Students</a></li>
                        <li><a href="all_course.php">All Courses</a></li>
                        <li><a href="assign.php">Course Registration</a></li>
                    </ul>

                    <form class="navbar-form navbar-left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>


<h2>Student Registration Form</h2>

<form action="student_added.php" method="POST" enctype="multipart/form-data">
    <div class="box">
        <hr>
        <div class="form-group">
            <label for="name">Enter Student's Name:</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Enter Student's Email:</label>
            <input id="email" type="email" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="pass">Enter Student's Id:</label>
            <input type="text" name="id" class="form-control">

        </div>
        <div class="form-group">
            <label for="address">Enter Student's Address:</label>
            <textarea id="address" name="address" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="cell">Enter Student's Cell:</label>
            <input id="cell" type="text" name="cell" class="form-control">
        </div>

        <div class="form-group">
            <label>Select Gender:</label>
            <input id="male" type="radio" name="gender" value="male">
            <label for="male">Male</label>

            <input id="female" type="radio" name="gender" value="female">
            <label for="female">Female</label>
        </div>

        <div class="form-group">
            <label for="image">Student Image</label>
            <input id="image" type="file" name="image" class="btn btn-default">
        </div>

        <div class="form-group">
            <label>Date of Birth:</label>
            <select name="day" class="btn btn-default">
                <option value="">Day</option>
                <?php
                    for($i = 1; $i < 32; $i++){
                        echo "<option value='$i'>$i</option>";
                     }

                ?>
            </select>
            <select name="month" class="btn btn-default">
                <option value="">Month</option>
                <?php
                    for($i = 1; $i < 13; $i++){
                        echo "<option value='$i'>$i</option>";
                    }

                ?>
            </select>
            <select name="year" class="btn btn-default">
                <option value="">Year</option>
                <?php
                    for($i = 1990; $i < 2018; $i++){
                        echo "<option value='$i'>$i</option>";
                    }

                ?>
            </select>
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Register" class="btn btn-success">
            <input type="reset" name="submit" value="Reset" class="btn btn-info">
        </div>
    </div>
</form>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <footer>Copyright &copy; Md. Arifur Rahman</footer>
        </div>
    </div>
</div>

</body>
</html>