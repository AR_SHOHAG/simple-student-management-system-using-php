
<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">


    <style>
        footer {
            padding: 1em;
            color: white;
            background-color: #1B4F72;
            clear: left;
            text-align: center;
        }

    </style>


</head>
<body>
<hr>
<h1 style="text-align: center;">Student Management System</h1>
<hr>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="add_course.php">Add Course</a></li>
                        <li><a href="add_student.php">Add Student</a></li>
                        <li><a href="all_student.php">All Students</a></li>
                        <li><a href="all_course.php">All Courses</a></li>
                        <li><a href="assign.php">Course Registration</a></li>
                    </ul>

                    <form class="navbar-form navbar-left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>


<h2>Add Course Form</h2>

	<form action="course_added.php" method="POST">
		<div class="box">
			<hr>
			<div class="form-group">
				<label >Course Name:</label>
				<input type="text" name="name" id="c_title" class="form-control">
			</div>
			<div class="form-group">
				<label >Course Code:</label>
				<input id="c_code" type="text" name="code" class="form-control">
			</div>

			<div class="form-group">
				<label >Course Credit:</label>
				<input id="c_credit" type="text" name="credit" class="form-control">
			</div>

			<div class="form-group">
				<input type="submit" name="submit" value="Add Course" class="btn btn-success">
				<input type="reset" name="submit" value="Reset" class="btn btn-info">
			</div>

		</div>
	</form>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <footer>Copyright &copy; Md. Arifur Rahman</footer>
            </div>
        </div>
    </div>

</body>
</html>