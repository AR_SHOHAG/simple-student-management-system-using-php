<?php
$db = new PDO('mysql:host=localhost;dbname=project1;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `student_info`";

$stmt = $db->query($query);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">


    <style>
        footer {
            padding: 1em;
            color: white;
            background-color: #1B4F72;
            clear: left;
            text-align: center;
        }

    </style>


</head>
<body>
<hr>
<h1 style="text-align: center;">Student Management System</h1>
<hr>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="add_course.php">Add Course</a></li>
                        <li><a href="add_student.php">Add Student</a></li>
                        <li><a href="all_student.php">All Students</a></li>
                        <li><a href="all_course.php">All Courses</a></li>
                        <li><a href="assign.php">Course Registration</a></li>
                    </ul>

                    <form class="navbar-form navbar-left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>


<h2>All the registered student list</h2>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class = "table table-bordered">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Id</th>
                        <th>Address</th>
                        <th>Cell</th>
                        <th>Gender</th>
                        <th>Image</th>
                        <th>Date of Birth</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                    $sl = 0;

                    foreach ($data as $user){
                        $sl++;



                ?>
                <tr>
                    <td><?php echo $sl?></td>
                    <td><?php echo $user['name'] ?></td>
                    <td><?php echo $user['email'] ?></td>
                    <td><?php echo $user['id'] ?></td>
                    <td><?php echo $user['address'] ?></td>
                    <td><?php echo '+880'.$user['cell'] ?></td>
                    <td><?php echo $user['gender'] ?></td>
                    <td style="width: 117px"><img src="uploads/<?php echo $user['image']; ?>" alt="" width="100" height="80" ></td>
                    <td><?php echo $user['dob'] ?></td>
                    <td>
                        <a href="details.php?id=<?php echo $user['id']?>"><button class = "btn-primary">Details</button></a><br>
                        <a href="modify.php?id=<?php echo $user['id']?>"><button class = "btn-default">Modify</button></a><br>
                        <a href="delete.php?id=<?php echo $user['id']?>"><button class = "btn-danger">Delete</button></a>
                    </td>

                </tr>

                <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <footer>Copyright &copy; Md. Arifur Rahman</footer>
        </div>
    </div>
</div>

</body>
</html>